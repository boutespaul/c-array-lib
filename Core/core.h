/**
 * @file core.h
 * @author Paul Boutes <paul.boutes@eisti.fr>
 * @brief Header of core file system
 * @date 2015-05-02
 */

//Copyright (C) 2015 Paul Boutes

#ifndef QUESTION_CORE_H
#define QUESTION_CORE_H
#include <stdarg.h>

typedef struct Object Object;

/**
 * @brief this is pointer to functions, used in forEach method as parameter
 * @param current The current element being processed in the array.
 * @param index The index of the current element being processed in the array.
 * @param array The array forEach() was called upon.
 */
typedef void (*callback)(int current, int index, int *array);

/**
 * @brief This is pointer to function, used in customSort method as parameter
 * @param self Refers to the array
 */
typedef void (*compare)(void *self);

/**
 * @brief This is pointer to function, pass to map method
 * @param current The current element being processed in the array
 * @param array The array map was called upon
 * @param index The index of the current element being processed in the array
 */
typedef int (*mapFunction)(int current, int *array, int index);

/**
 * @brief This is pointer to function, pass to reduce method
 * @param previousValue The value previously returned in the last invocation of the callback, or initialValue
 * @param currentValue The current element being processed in the array
 * @param index The index of the current element being processed in the array
 * @param array The array reduce was called upon
 */
typedef int (*reduceFunction)(int previousValue, int currentValue, int index, int *array);


/**
 * @brief This is the prototype object system. Here, we set prototype function which will use in by our Array object
 */
struct Object {
    int size;
    void (*display)(void *self);
    void (*destroy)(void *self);
    int (*init)(void *self, int length  );
    void (*push)(void *self, int a);
    int (*length)(void *self);
    void (*pop)(void *self);
    void (*unshift)(void *self, int a);
    void (*shift)(void *self);
    void (*forEach)(void *self, callback func);
    void (*reverse)(void *self);
    int (*indexOf)(void *self, int elm);
    void (*sort)(void *self);
    void (*customSort)(void *self, compare func);
    void* (*map)(void *self, mapFunction func);
    int (*reduce)(void *self, reduceFunction func);
};

void Object_destroy(void *self);
int Object_init(void *self, int length);
void *Object_new(size_t size, int length, Object proto);



#define new(T, N) Object_new(sizeof(T), N, T##Prototype);
#define _(N) prototype.N




#endif //QUESTION_CORE_H
