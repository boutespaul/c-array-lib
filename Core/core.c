
//Copyright (C) 2015 Paul Boutes

#include <stdio.h>
#include <stdlib.h>
#include "core.h"


void Object_destroy(void *self){
    Object *obj = self;
    if (obj){
        free(obj);
    }
}

int Object_init(void *self, int length){

    return 1;
}


int Object_length(void *self){

    return 0;
}

void Object_push(void *self, int a){

}


void *Object_new(size_t size, int length, Object proto) {

    if (!proto.push) proto.push = Object_push;
    if (!proto.length) proto.length = Object_length;

    Object *elm = calloc(1, size);
    *elm = proto;


    elm->size = length;

    if (!elm->init(elm, length)){
        elm->destroy(elm);
        return NULL;
    } else {
        return elm;
    }

}
