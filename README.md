# C Array Lib

## Author

* Paul Boutes [paul.boutes@eisti.fr]

This library is an implementation of JavaScript Array features. Right now, the library implement int Array, but more features will come soon.

## How to test

To test the library : 

    $ git@gitlab.etude.eisti.fr:boutespaul/c-array-lib.git
    $ cd c-array-lib/
    $ make
    $ ./bin/main
    


