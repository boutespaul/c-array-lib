
//Copyright (C) 2015 Paul Boutes

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "array.h"


int Array_init(void *self, int length){
    int i = 0;
    Array *array = self;
    array->size = length;
    array->data = malloc(length * sizeof(int));
    for (i = 0; i< length; ++i) {
        array->data[i] = 0;
    }
    return 1;
}

void Array_push(void *self, int a){
    Array *array = self;
    int length = array->size + 1;
    array->data = realloc(array->data, length * sizeof *array->data);
    array->size = length;
    array->data[length-1] = a;
}

int Array_length(void *self){
    Array *array = self;
    return array->size;
}

void Array_pop(void *self){
    Array *array = self;
    int length = array->size - 1;
    array->data = realloc(array->data, length * sizeof *array->data);
    array->size = length;
}

void Array_unshift(void *self, int a){
    Array *array = self;
    int length = array->size + 1;
    int i = 0;
    array->data = realloc(array->data, length * sizeof *array->data);
    for (i = length-1; i > 0; --i){
        array->data[i] = array->data[i-1];
    }
    array->data[0] = a;
    array->size = length;
}

void Array_shift(void *self){
    Array *array = self;
    int length = array->size - 1;
    int i;
    array->data = realloc(array->data, length * sizeof *array->data);
    for (i = 0; i < length; ++i){
        array->data[i] = array->data[i+1];
    }
    array->size = length;
}

void Array_forEach(void *self, callback func){
    Array *array = self;
    int i = 0;
    int length = array->size;
    for (i = 0; i < length; ++i){
        func(array->data[i], i, array->data);
    }
}

int Array_indexOf(void *self, int elm){
    Array *array = self;
    int i = 0;
    int index = -1;
    int length = array->size;
    int trouve = 0;
    while ((i < length) && (!trouve)) {
        if (array->data[i] == elm){
            index = i;
            trouve = 1;
        }
        ++i;
    }
    return index;
}


void Array_display(void *self){
    Array *array = self;
    array->_(forEach)(array, printArray);
}

void Array_destroy(void *self){
    Array *array = self;
    if (array){
        if (array->data){
            free(array->data);
        }
        free(array);
    }
}

void Array_reverse(void *self){
    Array *array = self;
    int length = array->size - 1;
    int tmp = 0;
    int start = 0;
    while (start < length){
        tmp = array->data[start];
        array->data[start] = array->data[length];
        array->data[length] = tmp;
        ++start;
        --length;
    }
}

void swap(int *array, int a, int b){
    int tmp = array[a];
    array[a] = array[b];
    array[b] = tmp;
}

void quicksort(int *array, int start, int end){
    int left = start - 1;
    int right = end + 1;
    int stop = 0;
    const int pivot = array[start];
    if (start >= end){
        return;
    }

    while (!stop){
        do --right; while (array[right] > pivot);
        do ++left; while (array[left] < pivot);
        if (left < right) {
            swap(array, left, right);
        } else {
            stop = 1;
        }
    }

    quicksort(array, start, right);
    quicksort(array, right + 1, end);
}

void Array_sort(void *self){
    Array *array = self;
    quicksort(array->data, 0, array->size - 1);
}

void Array_custom_sort(void *self, compare func){
    Array *array = self;
    func(array);
}

void printArray(int current, int index, int *array){
    printf("Array[%d] = %d\n", index, current);
}

void* Array_map(void *self, mapFunction func){
    Array *array = self;
    Array *res = new(Array, array->size);
    int i;
    for (i = 0; i < array->size; ++i){
        res->data[i] = func(array->data[i], array->data, i);
    }
    return res;
}

int Array_reduce(void *self, reduceFunction reduce){
    Array *array = self;
    int i;
    int reduceValue = array->data[0];
    for (i = 1; i < array->size; ++i){
       reduceValue = reduce(reduceValue, array->data[i], i, array->data);
    }
    return reduceValue;
}