/**
 * @file array.h
 * @author Paul Boutes <paul.boutes@eisti.fr>
 * @brief Array features
 * @date 2015-05-02
 */

//Copyright (C) 2015 Paul Boutes

#ifndef QUESTION_ARRAY_H
#define QUESTION_ARRAY_H

#include "../Core/core.h"

typedef struct Array Array;

/**
 * @brief Represents our Array object
 */
struct Array {
    /**< Our prototyping*/
    Object prototype;
    /**< Represents the int array*/
    int *data;
    /**< Length of our array*/
    int size;
};


/**
 * @brief implementation of quicksort algorithm
 * @param array Our array
 * @param start the index 0 of the array
 * @param end The last index of the array
 */
void quicksort(int *array, int start, int end);

/**
 * @brief Initialize the array
 * @param self Our object
 * @param length The new length for our array
 */
int Array_init(void *self, int length);


/**
 * @brief Adds one element to the end of an array and set the new length of the array.
 * @param self Our object
 * @param a Element to add to the end of the array
 */
void Array_push(void *self, int a);


/**
 * @brief Return the number of elements in an array
 * @param self Our object
 */
int Array_length(void *self);


/**
 * @brief Display the current array
 * @param self Our object
 */
void Array_display(void *self);


/**
 * @brief Destroy the array
 * @param self The object
 */
void Array_destroy(void *self);


/**
 * @brief removes the last element from an array
 * @param self The object
 */
void Array_pop(void *self);


/**
 * @brief adds one element to the beginning of an array and set the new length of the array.
 * @param self The object
 * @param a The element to add to the front of the array
 */
void Array_unshift(void *self, int a);


/**
 * @brief Removes the first element from an array and returns that element and set the new length of the array
 * @param self The object
 */
void Array_shift(void *self);


/**
 * @brief executes a provided function once per array element.
 * @param self The object
 * @param func Function to execute for each element
 */
void Array_forEach(void *self, callback func);


/**
 * @brief reverses an array in place.
 * @param self The object
 */
void Array_reverse(void *self);


/**
 * @brief Print the array value
 * @param current current value at the index of array
 * @param index current index of the array
 * @param array current array
 */
void printArray(int current, int index, int *array);


/**
 * @brief Return the first index at which a given element can be found in the array, or -1 if it is not present.
 * @param self The object
 * @param elm Element to locate in the array
 */
int Array_indexOf(void *self, int elm);


/**
 * @brief sorts the elements of an array in place by using quicksort algorithm
 * @param self The object
 */
void Array_sort(void *self);


/**
 * @brief sorts the elements of an array in place by using a custom sorting function
 * @param self The object
 * @param func Function used to sort the array
 */
void Array_custom_sort(void *self, compare func);


/**
 * @brief creates a new array with the results of calling a provided function on every element in this array.
 * @param self The object
 * @param func Function that produces an element of the new Array
 */
void* Array_map(void *self, mapFunction func);


/**
 * @brief applies a function against an accumulator and each value of the array (from left-to-right) has to reduce it to a single value.
 * @param self The object
 * @param func Function to execute on each value in the array
 */
int Array_reduce(void *self, reduceFunction func);


/**
 * @brief Represent the ArrayPrototype
 */
extern Object ArrayPrototype;

#endif //QUESTION_ARRAY_H
