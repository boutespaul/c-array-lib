
//Copyright (C) 2015 Paul Boutes

#include <stdio.h>

#include "Util/array.h"

Object ArrayPrototype = {
        .init = Array_init,
        .length = Array_length,
        .push = Array_push,
        .display = Array_display,
        .destroy = Array_destroy,
        .pop = Array_pop,
        .unshift = Array_unshift,
        .shift = Array_shift,
        .forEach = Array_forEach,
        .reverse = Array_reverse,
        .indexOf = Array_indexOf,
        .sort = Array_sort,
        .customSort = Array_custom_sort,
        .map = Array_map,
        .reduce = Array_reduce
};



void add(int current, int index, int *data){
    int n = ++current;
    data[index] = n;
}


void testUnshift(void *self){

    Array *array = self;

    printf("Unshift 4 to the array \n\n");

    // Add 1 element to the beginning of the array
    array->_(unshift)(array, 4);

    array->_(display)(array);

    printf("Length = %d \n\n", array->size);
}

void testPush(void *self){
    Array *array = self;

    printf("Push 20 to the array \n\n");

    // Add 1 element to the end of the array
    array->_(push)(array,20);

    array->_(display)(array);

    printf("Length = %d \n\n", array->size);
}


void testPop(void *self){
    Array *array = self;

    printf("Remove the last element \n\n");

    // Remove the last element of the array
    array->_(pop)(array);

    array->_(display)(array);

    printf("Length = %d \n\n", array->size);

}

void testReverse(void *self){
    Array *array = self;

    printf("Reverse the array \n\n");

    // Reverse the array
    array->_(reverse)(array);

    array->_(display)(array);

    printf("Length = %d \n\n", array->size);
}


void testForeach(void *self){
    Array *array = self;

    printf("Foreach on the array \n\n");

    // Executes a provided function once per array element.
    array->_(forEach)(array, add);

    array->_(display)(array);

    printf("Length = %d \n\n", array->size);

}

void testIndexOf(void *self){
    Array *array = self;
    int index;
    printf("Index of 5 \n\n");

    // Returns the first index at which a given element can be found in the array, or -1 if it is not present.
    index = array->_(indexOf)(array, 5);

    array->_(display)(array);

    printf("Length = %d \n\n", array->size);

    printf("Index of 5 = %d \n", index);
}

int doubleMap(int current, int *array, int index){
    return current * 2;
}



int main(int argc, char **argv){
    int length;

    printf("Start \n\n");


    // Declare New array Object
    Array *array = new(Array, 1);

    array->_(push)(array, 10);

    array->_(display)(array);



    return 0;
}
